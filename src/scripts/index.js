import '../styles/index.scss';



/**
 * Documentation for the slider
 * https://bxslider.com/examples/auto-show-start-stop-controls/ 
 */

/**
* Documentation for the Lightbox - Fancybox
* See the section 5. Fire plugin using jQuery selector.
* http://fancybox.net/howto
*/

/**
 * Boostrap Modal (For the Learn More button)
 * https://getbootstrap.com/docs/4.0/components/modal/
 */
// Render all the photos from the API
function renderPhotos(photos) {
    $.each(photos, (i, photo) => {
        document.getElementById('row').innerHTML += createCard(photo);
    });
}
function createCard(photo) {
    return `
    <div class="col-md-4">
    <div class="card mh-100" style="18rem;">
    <div class="card-img" >
    <div class="container-fluid content-row">
    <img class="card-img-top" src="${photo.src.portrait}"/>
    </div>
    <div class="card-body">
    <h5>${photo.photographer}</h5>
    </div>
    </div>
    </div>
    `;
}
//***********************/
var input = "car";//Initial value forr picture
var numberOfPages = 5;//Initial value forr number of pages

// InsertPhoto function get the photo and number of the pages
function InsertPhoto(query, pages) {
    const apiKey = '563492ad6f917000010000019dbfd6170a8143baaf1774afa0de573d';
    const apiUrl = `https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?query=${query}&per_page=${pages}&page=1`;
    $.ajax({
        headers: {
            'Authorization': `Bearer ${apiKey}`
        },
        url: apiUrl,
        type: 'GET'
    }).done(data => {
        renderPhotos(data.photos);
    });
};

//get valu from text box and by clicking the search button 
$("#submitInput").click(function () {
    var input = $("#serachInput");
    var numberOfPages = $("#pagenumber");
    $(".col-md-4").remove(); // remove after each load
    InsertPhoto(input.val(), numberOfPages.val());
});

//*****************/
$("#pagenumber").change(function () {
    var input = $("#serachInput");
    var numberOfPages = $("#pagenumber");
    $(".col-md-4").remove();
    InsertPhoto(input.val(), numberOfPages.val());

});

$("document").ready(function () {

    InsertPhoto(input, numberOfPages);
});
//*****************/
window.helloWorld = function () {
    console.log('HEllooOooOOo!');
};
